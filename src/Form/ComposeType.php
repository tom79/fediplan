<?php /** @noinspection PhpTranslationKeyInspection */

/**
 * Created by fediplan.
 * User: tom79
 * Date: 08/08/19
 * Time: 14:57
 */

namespace App\Form;


use App\Security\MastodonAccount;
use App\SocialEntity\Compose;
use DateTime;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ComposeType extends AbstractType
{


    private Security $securityContext;
    private $translator;

    public function __construct(Security $securityContext, Translator $translator)
    {
        $this->securityContext = $securityContext;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /**@var $user MastodonAccount */
        $user = $options['user'];

        if ($user->getDefaultSensitivity()) {
            $checkbox = [
                'required' => false,
                'attr' => ['checked' => 'checked'],
                'label' => 'page.schedule.form.sensitive',
                'translation_domain' => 'fediplan'
            ];
        } else {
            $checkbox = ['required' => false, 'label' => 'page.schedule.form.sensitive',
                'translation_domain' => 'fediplan'];
        }

        $builder->add('content_warning', TextType::class, [
            'required' => false,
            'label' => 'page.schedule.form.content_warning',
            'translation_domain' => 'fediplan']);
        $builder->add('content', TextareaType::class, [
            'required' => false,
            'label' => 'page.schedule.form.content',
            'translation_domain' => 'fediplan']);
        $builder->add('visibility', ChoiceType::class,
            [
                'choices' => [
                    'status.visibility.public' => 'public',
                    'status.visibility.unlisted' => 'unlisted',
                    'status.visibility.private' => 'private',
                    'status.visibility.direct' => 'direct',
                ],
                'data' => $user->getDefaultVisibility(),
                'label' => 'page.schedule.form.visibility',
                'translation_domain' => 'fediplan']);
        $builder->add('attach_poll', HiddenType::class, ['required' => true, 'empty_data' => 0]);

        $builder->add('timeZone', TimezoneType::class,
            [
                'label' => 'page.schedule.form.timeZone',
                'translation_domain' => 'fediplan']);
        $builder->add('sensitive', CheckboxType::class, $checkbox);
        $builder->add('scheduled_at', DateTimeType::class, [
            'widget' => 'single_text',
            "data" => new DateTime(),
            'label' => 'page.schedule.form.scheduled_at',
            'translation_domain' => 'fediplan']);

        $builder->add('poll_options', CollectionType::class,
            [
                'entry_type' => PollOptionType::class,
                'by_reference' => false,
                'allow_add' => true,
                'prototype' => true,
                'entry_options' => ['label' => false],
                'allow_delete' => true,
                'required' => false,
            ]);
        $builder->add('poll_multiple', CheckboxType::class,
            ['required' => false, 'label' => 'page.schedule.form.multiple',
                'translation_domain' => 'fediplan']);
        $builder->add('poll_expires_at', ChoiceType::class,
            [
                'choices' => [
                    $this->translator->trans('poll.duration_m', ['minutes' => 5], 'fediplan') => 5 * 60,
                    $this->translator->trans('poll.duration_m', ['minutes' => 30], 'fediplan') => 30 * 60,
                    $this->translator->trans('poll.duration_h', ['hours' => 1], 'fediplan') => 60 * 60,
                    $this->translator->trans('poll.duration_h', ['hours' => 6], 'fediplan') => 6 * 60 * 60,
                    $this->translator->trans('poll.duration_d', ['days' => 1], 'fediplan') => 24 * 60 * 60,
                    $this->translator->trans('poll.duration_d', ['days' => 3], 'fediplan') => 3 * 24 * 60 * 60,
                    $this->translator->trans('poll.duration_d', ['days' => 7], 'fediplan') => 7 * 24 * 60 * 60,

                ],
                'data' => 24 * 60 * 60,
                'required' => false,
                'label' => 'page.schedule.form.end_in',
                'translation_domain' => 'fediplan']);
        $builder->add('Send', SubmitType::class,
            ['attr' => ['class' => "btn btn-primary "],
                'label' => 'page.schedule.form.send',
                'translation_domain' => 'fediplan']);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Compose::class,
            'translation_domain' => 'fediplan',
            'user' => null
        ]);
    }

}