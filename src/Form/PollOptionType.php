<?php
/**
 * Created by fediplan.
 * User: tom79
 */

namespace App\Form;


use App\SocialEntity\Instance;
use App\SocialEntity\PollOption;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PollOptionType extends AbstractType
{


    private Security $securityContext;
    private Instance $instance;

    public function __construct(Security $securityContext, RequestStack $requestStack)
    {
        $this->securityContext = $securityContext;
        $this->instance = $requestStack->getSession()->get('instance');
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $max_char = $this->instance->getConfiguration()->getPolls()->getMaxCharactersPerOption();
        $builder->add('title', TextType::class,
            [
                'required' => false,
                'attr' => ['class' => 'form-control', 'maxlength' => $max_char],
                'label' => 'page.schedule.form.poll_item',
            ]);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PollOption::class,
            'translation_domain' => 'fediplan'
        ]);
    }

}