<?php

namespace App\Security;

use App\SocialEntity\Client;
use App\SocialEntity\CustomField;
use App\SocialEntity\Emoji;
use Symfony\Component\Security\Core\User\UserInterface;

class MastodonAccount implements UserInterface
{
    private string $acct;
    private string $id;

    private string $account_id;

    private string $username;

    private string $display_name;

    private bool $locked;

    private \DateTime $created_at;

    private int $followers_count;

    private int $following_count;

    private int $statuses_count;

    private string $note;

    private string $url;

    private string $avatar;

    private string $avatar_static;

    private string $header;

    private string $header_static;

    private MastodonAccount $moved;

    private bool $bot;

    private string $instance;

    private Client $client;

    private string $token;

    private array $Fields;
    /** @var Emoji[] */
    private array $Emojis;

    private string $default_sensitivity;

    private string $default_visibility;


    public function __construct()
    {
        $this->Fields = array();
        $this->Emojis = array();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccountId(): ?string
    {
        return $this->account_id;
    }

    public function setAccountId(string $account_id): self
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->display_name;
    }

    public function setDisplayName(?string $display_name): self
    {
        $this->display_name = $display_name;

        return $this;
    }

    public function getLocked(): ?bool
    {
        return $this->locked;
    }

    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getFollowersCount(): ?int
    {
        return $this->followers_count;
    }

    public function setFollowersCount(int $followers_count): self
    {
        $this->followers_count = $followers_count;

        return $this;
    }

    public function getFollowingCount(): ?int
    {
        return $this->following_count;
    }

    public function setFollowingCount(int $following_count): self
    {
        $this->following_count = $following_count;

        return $this;
    }

    public function getStatusesCount(): ?int
    {
        return $this->statuses_count;
    }

    public function setStatusesCount(int $statuses_count): self
    {
        $this->statuses_count = $statuses_count;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getAvatarStatic(): ?string
    {
        return $this->avatar_static;
    }

    public function setAvatarStatic(?string $avatar_static): self
    {
        $this->avatar_static = $avatar_static;

        return $this;
    }

    public function getHeader(): ?string
    {
        return $this->header;
    }

    public function setHeader(?string $header): self
    {
        $this->header = $header;

        return $this;
    }

    public function getHeaderStatic(): ?string
    {
        return $this->header_static;
    }

    public function setHeaderStatic(?string $header_static): self
    {
        $this->header_static = $header_static;

        return $this;
    }

    public function getMoved(): ?self
    {
        return $this->moved;
    }

    public function setMoved(?self $moved): self
    {
        $this->moved = $moved;

        return $this;
    }

    public function getBot(): ?bool
    {
        return $this->bot;
    }

    public function setBot(?bool $bot): self
    {
        $this->bot = $bot;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        // set (or unset) the owning side of the relation if necessary
        $newAccount = $client === null ? null : $this;
        if ($newAccount !== $client->getAccount()) {
            $client->setAccount($newAccount);
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function __toString()
    {
        return $this->getAcct() . "@" . $this->getInstance();
    }

    public function getAcct(): ?string
    {
        return $this->acct;
    }

    public function setAcct(string $acct): self
    {
        $this->acct = $acct;

        return $this;
    }

    public function getInstance(): ?string
    {
        return $this->instance;
    }

    public function setInstance(string $instance): self
    {
        $this->instance = $instance;

        return $this;
    }


    public function getFields(): array
    {
        return $this->Fields;
    }

    public function addField(CustomField $field): self
    {
        if (in_array($field, $this->Fields) !== false) {
            $this->Fields[] = $field;
            $field->setMastodonAccount($this);
        }
        return $this;
    }

    public function removeField(CustomField $field): self
    {

        if (($key = array_search($field, $this->Fields)) !== false) {
            unset($this->Fields[$key]);
            // set the owning side to null (unless already changed)
            if ($field->getMastodonAccount() === $this) {
                $field->setMastodonAccount(null);
            }
        }
        return $this;
    }

    public function getEmojis(): array
    {
        return $this->Emojis;
    }

    public function addEmoji(Emoji $emoji): self
    {
        if (in_array($emoji, $this->Emojis) !== false) {
            $this->Emojis[] = $emoji;
            $emoji->setMastodonAccount($this);
        }
        return $this;
    }

    public function removeEmoji(Emoji $emoji): self
    {
        if (($key = array_search($emoji, $this->Emojis)) !== false) {
            unset($this->Emojis[$key]);
            // set the owning side to null (unless already changed)
            if ($emoji->getMastodonAccount() === $this) {
                $emoji->setMastodonAccount(null);
            }
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function getDefaultSensitivity(): mixed
    {
        return $this->default_sensitivity;
    }

    /**
     * @param mixed $default_sensitivity
     */
    public function setDefaultSensitivity($default_sensitivity): void
    {
        $this->default_sensitivity = $default_sensitivity;
    }

    /**
     * @return mixed
     */
    public function getDefaultVisibility()
    {
        return $this->default_visibility;
    }

    /**
     * @param mixed $default_visibility
     */
    public function setDefaultVisibility($default_visibility): void
    {
        $this->default_visibility = $default_visibility;
    }
    /**
     * @var list<string> The user roles
     */
    private $roles = [];


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->acct;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
