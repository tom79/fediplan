<?php


namespace App\SocialEntity;


use DateTime;

class Poll
{
    private string $id;
    private DateTime $expires_at;
    private bool $expired;
    private bool $multiple;
    private int $votes_count;
    private int $voters_count;
    private bool $voted;
    /** @var int[] */
    private array $own_votes;
    /** @var PollOption[] */
    private array $options;
    /** @var Emoji[] */
    private array $emojis;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return DateTime
     */
    public function getExpiresAt(): DateTime
    {
        return $this->expires_at;
    }

    /**
     * @param DateTime $expires_at
     */
    public function setExpiresAt(DateTime $expires_at): void
    {
        $this->expires_at = $expires_at;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->expired;
    }

    /**
     * @param bool $expired
     */
    public function setExpired(bool $expired): void
    {
        $this->expired = $expired;
    }

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    /**
     * @param bool $multiple
     */
    public function setMultiple(bool $multiple): void
    {
        $this->multiple = $multiple;
    }

    /**
     * @return int
     */
    public function getVotesCount(): int
    {
        return $this->votes_count;
    }

    /**
     * @param int $votes_count
     */
    public function setVotesCount(int $votes_count): void
    {
        $this->votes_count = $votes_count;
    }

    /**
     * @return int
     */
    public function getVotersCount(): int
    {
        return $this->voters_count;
    }

    /**
     * @param int $voters_count
     */
    public function setVotersCount(int $voters_count): void
    {
        $this->voters_count = $voters_count;
    }

    /**
     * @return bool
     */
    public function isVoted(): bool
    {
        return $this->voted;
    }

    /**
     * @param bool $voted
     */
    public function setVoted(bool $voted): void
    {
        $this->voted = $voted;
    }

    /**
     * @return int[]
     */
    public function getOwnVotes(): array
    {
        return $this->own_votes;
    }

    /**
     * @param int[] $own_votes
     */
    public function setOwnVotes(array $own_votes): void
    {
        $this->own_votes = $own_votes;
    }

    /**
     * @return PollOption[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param PollOption[] $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    /**
     * @return Emoji[]
     */
    public function getEmojis(): array
    {
        return $this->emojis;
    }

    /**
     * @param Emoji[] $emojis
     */
    public function setEmojis(array $emojis): void
    {
        $this->emojis = $emojis;
    }


}