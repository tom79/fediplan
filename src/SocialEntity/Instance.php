<?php

namespace App\SocialEntity;

class Statuses {
    private int $max_characters = 500;
    private int $max_media_attachments = 4;
    private int $characters_reserved_per_url = 23;
    public function getMaxCharacters(): int
    {
        return $this->max_characters;
    }

    public function setMaxCharacters(int $max_characters): void
    {
        $this->max_characters = $max_characters;
    }

    public function getMaxMediaAttachments(): int
    {
        return $this->max_media_attachments;
    }

    public function setMaxMediaAttachments(int $max_media_attachments): void
    {
        $this->max_media_attachments = $max_media_attachments;
    }

    public function getCharactersReservedPerUrl(): int
    {
        return $this->characters_reserved_per_url;
    }

    public function setCharactersReservedPerUrl(int $characters_reserved_per_url): void
    {
        $this->characters_reserved_per_url = $characters_reserved_per_url;
    }
}

class MediaAttachments {
    private array $supported_mime_types = ["image/jpeg","image/png","image/gif","image/heic","image/heif","image/webp","image/avif","video/webm","video/mp4","video/quicktime","video/ogg","audio/wave","audio/wav","audio/x-wav","audio/x-pn-wave","audio/vnd.wave","audio/ogg","audio/vorbis","audio/mpeg","audio/mp3","audio/webm","audio/flac","audio/aac","audio/m4a","audio/x-m4a","audio/mp4","audio/3gpp","video/x-ms-asf"];
    private int $image_size_limit = 16777216;
    private int $image_matrix_limit = 33177600;
    private int $video_size_limit = 103809024;
    private int $video_frame_rate_limit = 120;
    private int $video_matrix_limit = 8294400;
    public function getSupportedMimeTypes(): array
    {
        return $this->supported_mime_types;
    }

    public function setSupportedMimeTypes(array $supported_mime_types): void
    {
        $this->supported_mime_types = $supported_mime_types;
    }

    public function getSupportedFiles() : string {
        $values = "/(\.|\/)(gif|jpe?g|apng|png|mp4|mp3|avi|mov|webm|wmv|flv|wav|ogg)$/i";
        if(isset($this->supported_mime_types) && count($this->supported_mime_types) >0) {
            $values = "/(\.|\/)(";
            foreach ($this->supported_mime_types as $value) {
                $cleanedValue = preg_replace("#(image/)|(video/)|(audio/)#","",$value,);
                if(!str_contains($cleanedValue, '.') && !str_contains($cleanedValue, '-')) {
                    $values .= $cleanedValue.'|';
                }
            }
            $values .= "jpg)$/i";
        }
        return $values;
    }
    public function getImageSizeLimit(): int
    {
        return $this->image_size_limit;
    }

    public function setImageSizeLimit(int $image_size_limit): void
    {
        $this->image_size_limit = $image_size_limit;
    }

    public function getImageMatrixLimit(): int
    {
        return $this->image_matrix_limit;
    }

    public function setImageMatrixLimit(int $image_matrix_limit): void
    {
        $this->image_matrix_limit = $image_matrix_limit;
    }

    public function getVideoSizeLimit(): int
    {
        return $this->video_size_limit;
    }

    public function setVideoSizeLimit(int $video_size_limit): void
    {
        $this->video_size_limit = $video_size_limit;
    }

    public function getVideoFrameRateLimit(): int
    {
        return $this->video_frame_rate_limit;
    }

    public function setVideoFrameRateLimit(int $video_frame_rate_limit): void
    {
        $this->video_frame_rate_limit = $video_frame_rate_limit;
    }

    public function getVideoMatrixLimit(): int
    {
        return $this->video_matrix_limit;
    }

    public function setVideoMatrixLimit(int $video_matrix_limit): void
    {
        $this->video_matrix_limit = $video_matrix_limit;
    }
}


class Polls {
    private int $max_options = 4;
    private int $max_characters_per_option = 50;
    private int $min_expiration = 300;
    private int $max_expiration = 2629746;

    public function getMaxOptions(): int
    {
        return $this->max_options;
    }

    public function setMaxOptions(int $max_options): void
    {
        $this->max_options = $max_options;
    }

    public function getMaxCharactersPerOption(): int
    {
        return $this->max_characters_per_option;
    }

    public function setMaxCharactersPerOption(int $max_characters_per_option): void
    {
        $this->max_characters_per_option = $max_characters_per_option;
    }

    public function getMinExpiration(): int
    {
        return $this->min_expiration;
    }

    public function setMinExpiration(int $min_expiration): void
    {
        $this->min_expiration = $min_expiration;
    }

    public function getMaxExpiration(): int
    {
        return $this->max_expiration;
    }

    public function setMaxExpiration(int $max_expiration): void
    {
        $this->max_expiration = $max_expiration;
    }

}

class Configuration {
    private Statuses $statuses;
    private MediaAttachments $mediaAttachments;

    public function getStatuses(): Statuses
    {
        return $this->statuses;
    }

    public function setStatuses(Statuses $statuses): void
    {
        $this->statuses = $statuses;
    }

    public function getMediaAttachments(): MediaAttachments
    {
        return $this->mediaAttachments;
    }

    public function setMediaAttachments(MediaAttachments $mediaAttachments): void
    {
        $this->mediaAttachments = $mediaAttachments;
    }

    public function getPolls(): Polls
    {
        return $this->polls;
    }

    public function setPolls(Polls $polls): void
    {
        $this->polls = $polls;
    }
    private Polls $polls;

}


class Instance
{
    private Configuration $configuration;

    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    public function setConfiguration(Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }

}


