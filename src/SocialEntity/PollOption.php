<?php


namespace App\SocialEntity;


class PollOption
{
    private ?string $title = null;
    private ?int $votes_count = null;


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }


    public function getVotesCount(): ?int
    {
        return $this->votes_count;
    }


    public function setVotesCount(?int $votes_count): void
    {
        $this->votes_count = $votes_count;
    }


    public function __toString()
    {
        return $this->title;
    }
}