<?php

namespace App\SocialEntity;

use DateTime;



class Compose
{

    private string $id;
    private ?string $content_warning = null;
    private ?string $content = null;

    private string $visibility;
    private DateTime $created_at;
    private DateTime $scheduled_at;
    private DateTime $sent_at;
    private bool $sensitive;
    private ?string $in_reply_to_id = null;

    private string $timeZone;
    /** @var PollOption[] */
    private ?array $poll_options = null;
    private ?int  $poll_expires_at = null;
    private ?bool $poll_multiple = null;

    public function getAttachPoll(): ?bool
    {
        return $this->attach_poll;
    }

    public function setAttachPoll(?bool $attach_poll): void
    {
        $this->attach_poll = $attach_poll;
    }
    private ?bool $attach_poll = null;

    public function __construct()
    {
        $this->poll_options = array();
    }


    public function getTimeZone(): string
    {
        return $this->timeZone;
    }


    public function setTimeZone($timeZone): void
    {
        $this->timeZone = $timeZone;
    }

    public function getTotalMedia()
    {
    }

    public function getSent()
    {
        return ($this->sent_at != null && !empty($this->sent_at));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContentWarning(): ?string
    {
        return $this->content_warning;
    }

    public function setContentWarning(?string $content_warning): self
    {
        $this->content_warning = $content_warning;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getVisibility(): ?string
    {
        return $this->visibility;
    }

    public function setVisibility(string $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }


    public function getSensitive(): bool
    {
        return $this->sensitive;
    }

    public function setSensitive(bool $sensitive): void
    {
        $this->sensitive = $sensitive;
    }


    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getScheduledAt(): ?DateTime
    {
        return $this->scheduled_at;
    }

    public function setScheduledAt(?DateTime $scheduled_at): self
    {
        $this->scheduled_at = $scheduled_at;

        return $this;
    }

    public function getInReplyToId(): ?string
    {
        return $this->in_reply_to_id;
    }

    public function setInReplyToId(?string $in_reply_to_id): self
    {
        $this->in_reply_to_id = $in_reply_to_id;

        return $this;
    }



    public function getPollOptions(): ?array
    {
        return $this->poll_options;
    }


    public function setPollOptions(?array $poll_options): void
    {
        $this->poll_options = $poll_options;
    }

    public function getPollExpiresAt(): ?int
    {
        return $this->poll_expires_at;
    }

    public function setPollExpiresAt(?int $poll_expires_at): void
    {
        $this->poll_expires_at = $poll_expires_at;
    }

    public function isPollMultiple(): ?bool
    {
        return $this->poll_multiple;
    }

    /**
     * @param bool $poll_multiple
     */
    public function setPollMultiple(?bool $poll_multiple): void
    {
        $this->poll_multiple = $poll_multiple;
    }

}
