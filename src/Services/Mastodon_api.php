<?php /** @noinspection PhpUnused */
/** @noinspection DuplicatedCode */

/**
 * Created by fediplan.
 * User: tom79
 * Date: 08/08/19
 * Time: 15:02
 */

namespace App\Services;


use App\Security\MastodonAccount;
use App\Services\Curl as Curl;
use App\SocialEntity\Application;
use App\SocialEntity\Attachment;
use App\SocialEntity\Configuration;
use App\SocialEntity\CustomField;
use App\SocialEntity\Emoji;
use App\SocialEntity\Instance;
use App\SocialEntity\MediaAttachments;
use App\SocialEntity\Mention;
use App\SocialEntity\Notification;
use App\SocialEntity\Poll;
use App\SocialEntity\PollOption;
use App\SocialEntity\Polls;
use App\SocialEntity\Status;
use App\SocialEntity\Statuses;
use App\SocialEntity\Tag;
use CURLFile;
use DateTime;
use Exception;
use function json_decode;

/**
 * Class Mastodon_api
 *
 * PHP version 7.1
 *
 * Mastodon     https://mastodon.social/
 * API LIST     https://docs.joinmastodon.org/api/
 *
 * @author      KwangSeon Yun   <middleyks@hanmail.net>
 * @copyright   KwangSeon Yun
 * @license     https://raw.githubusercontent.com/yks118/Mastodon-api-php/master/LICENSE     MIT License
 * @link        https://github.com/yks118/Mastodon-api-php
 * @link        https://framagit.org/tom79/fediplan/-/blob/master/src/Services/Mastodon_api.php
 *
 * This api code is now maintained for Fediplan - It can be used separately but it needs entity classes in SocialEntity for working
 */
class Mastodon_api
{
    private $mastodon_url = '';
    private $client_id = '';
    private $client_secret = '';

    private $token = array();
    private $scopes = array();

    public function __construct()
    {
    }

    public function __destruct()
    {
    }

    /**
     * set_url
     *
     * @param string $path
     */
    public function set_url(string $path): void
    {
        $this->mastodon_url = $path;
    }

    /**
     * set_client
     *
     * @param string $id
     * @param string $secret
     */
    public function set_client(string $id, string $secret): void
    {
        $this->client_id = $id;
        $this->client_secret = $secret;
    }

    /**
     * set_token
     *
     * @param string $token
     * @param string $type
     */
    public function set_token(string $token, string $type): void
    {
        $this->token['access_token'] = $token;
        $this->token['token_type'] = $type;
    }

    /**
     * set_scopes
     *
     * @param array $scopes read / write / follow
     */
    public function set_scopes(array $scopes): void
    {
        $this->scopes = $scopes;
    }

    /**
     * create_app
     *
     * @param string $client_name
     * @param array $scopes read / write / follow
     * @param string $redirect_uris
     * @param string $website
     *
     * @return  array       $response
     *          int         $response['id']
     *          string      $response['redirect_uri']
     *          string      $response['client_id']
     *          string      $response['client_secret']
     */
    public function create_app(string $client_name, array $scopes = array(), string $redirect_uris = '', string $website = ''): array
    {
        $parameters = array();

        if (count($scopes) == 0) {
            if (count($this->scopes) == 0) {
                $scopes = array('read', 'write', 'follow');
            } else {
                $scopes = $this->scopes;
            }
        }

        $parameters['client_name'] = $client_name;
        $parameters['scopes'] = implode(' ', $scopes);
        if (empty($redirect_uris)) {
            $parameters['redirect_uris'] = 'urn:ietf:wg:oauth:2.0:oob';
        } else {
            $parameters['redirect_uris'] = $redirect_uris;
        }

        if ($website) {
            $parameters['website'] = $website;
        }

        $response = $this->_post('/api/v1/apps', $parameters);
        if (isset($response['html']['client_id'])) {
            $this->client_id = $response['html']['client_id'];
            $this->client_secret = $response['html']['client_secret'];
        }

        return $response;
    }

    /**
     * _post
     *
     * HTTP API post
     *
     * @param string $url
     * @param array $parameters
     *
     * @return  array       $response
     */
    private function _post(string $url, array $parameters = array()): array
    {

        $params["method"] = "POST";
        // set access_token
        if (isset($this->token['access_token'])) {
            $params['headers'] = array(
                'Authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']
            );
        }
        $params['body'] = $parameters;
        $url = $this->mastodon_url . $url;
        return $this->get_content_remote($url, $params);
    }

    /**
     * get_content_remote_get
     *
     * @param string $url
     * @param array $parameters
     *
     * @return  array       $data
     */
    public function get_content_remote(string $url, array $parameters = array()): array
    {
        $data = array();

        // set USERAGENT
        $parameters['headers']['User-Agent'] = 'Mozilla/5.0 (X11; Linux i686; rv:126.0) Gecko/20100101 Firefox/126.0';

        $curl = new Curl();
        $response = null;
        if (isset($parameters["method"]) && $parameters['method'] == "POST") {
            $parameters['headers']['content-type'] = 'application/json';
        }
        foreach ($parameters['headers'] as $key => $value) {
            $curl->setHeader($key, $value);
        }
        if (isset($parameters["method"]) && $parameters['method'] == "POST") {
            $response = $curl->post($url, $parameters['body']);
        }else if (isset($parameters["method"]) && $parameters['method'] == "GET") {
            $response = $curl->get($url, $parameters['body']);
        }else if (isset($parameters["method"]) && $parameters['method'] == "PUT") {
            $response = $curl->put($url, $parameters['body']);
        }else if (isset($parameters["method"]) && $parameters['method'] == "PATCH") {
            $response = $curl->patch($url, $parameters['body']);
        }else if (isset($parameters["method"]) && $parameters['method'] == "DELETE") {
            $response = $curl->delete($url);
        }

        $min_id = null;
        $max_id = null;
        if ($response->response_headers) {
            foreach ($response->response_headers as $value) {

                if (strpos($value, 'Link: ') !== false) {
                    preg_match(
                        "/min_id=([0-9a-zA-Z]+)/",
                        $value,
                        $matches
                    );
                    if ($matches) {
                        $min_id = $matches[1];
                    }
                }
                if (strpos($value, 'Link: ') !== false) {
                    preg_match(
                        "/max_id=([0-9a-zA-Z]+)/",
                        $value,
                        $matches
                    );
                    if ($matches) {
                        $max_id = $matches[1];
                    }
                }
            }
        }
        $data['min_id'] = $min_id;
        $data['max_id'] = $max_id;
        if ($response->error) {
            $data['error'] = $response->error;
            $data['error_code'] = $response->error_code;
            $data['error_message'] = $response->error_message;
            if ($response->response && isset(json_decode($response->response, true)['error']))
                $data['error_message'] = json_decode($response->response, true)['error'];
        } else {
            $data['response_headers'] = $response->response_headers;
            $data['response'] = json_decode($response->response, true);
        }
        return $data;
    }

    /**
     * login
     *
     * @param string $id E-mail Address
     * @param string $password Password
     *
     * @return  array       $response
     *          string      $response['access_token']
     *          string      $response['token_type']         bearer
     *          string      $response['scope']              read
     *          int         $response['created_at']         time
     */
    public function login(string $id, string $password): array
    {
        $parameters = array();
        $parameters['client_id'] = $this->client_id;
        $parameters['client_secret'] = $this->client_secret;
        $parameters['grant_type'] = 'password';
        $parameters['username'] = $id;
        $parameters['password'] = $password;

        if (count($this->scopes) == 0) {
            $parameters['scope'] = implode(' ', array('read', 'write', 'follow'));
        } else {
            $parameters['scope'] = implode(' ', $this->scopes);
        }
        $response = $this->_post('/oauth/token', $parameters);

        if (isset($response['html']['access_token'])) {
            $this->token['access_token'] = $response['html']['access_token'];
            $this->token['token_type'] = $response['html']['token_type'];
        }

        return $response;
    }

    /**
     * login
     *
     * @param string $code Authorization code
     * @param string $redirect_uri
     *
     * @return  array       $response
     *          string      $response['access_token']
     *          string      $response['token_type']         bearer
     *          string      $response['scope']              read
     *          int         $response['created_at']         time
     */
    public function loginAuthorization(string $code, string $redirect_uri = ''): array
    {
        $parameters = array();
        $parameters['client_id'] = $this->client_id;
        $parameters['client_secret'] = $this->client_secret;
        if (empty($redirect_uri)) {
            $parameters['redirect_uri'] = 'urn:ietf:wg:oauth:2.0:oob';
        } else {
            $parameters['redirect_uri'] = $redirect_uri;
        }
        $parameters['grant_type'] = 'authorization_code';
        $parameters['code'] = $code;
        $response = $this->_post('/oauth/token', $parameters);
        if (isset($response['html']['access_token'])) {
            $this->token['access_token'] = $response['html']['access_token'];
            $this->token['token_type'] = $response['html']['token_type'];
        }
        return $response;
    }

    /**
     * getAuthorizationUrl
     *
     * @param string $redirect_uri
     *
     * @return  string       $response Authorization code
     */
    public function getAuthorizationUrl(string $redirect_uri = ''): string
    {
        if (empty($redirect_uri))
            $redirect_uri = 'urn:ietf:wg:oauth:2.0:oob';
        if (count($this->scopes) == 0) {
            $scopes = array('read', 'write', 'follow');
        } else {
            $scopes = $this->scopes;
        }
        $scope_uri = "";
        foreach ($scopes as $scope)
            $scope_uri .= $scope . " ";
        return $this->mastodon_url . '/oauth/authorize?' .
            "client_id=" . $this->client_id . "&redirect_uri=" . $redirect_uri .
            "&response_type=code&scope=" . trim($scope_uri);
    }

    /**
     * accounts
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     *          int         $response['id']
     *          string      $response['username']
     *          string      $response['acct']
     *          string      $response['display_name']           The name to display in the user's profile
     *          bool        $response['locked']
     *          string      $response['created_at']
     *          int         $response['followers_count']
     *          int         $response['following_count']
     *          int         $response['statuses_count']
     *          string      $response['note']                   A new biography for the user
     *          string      $response['url']
     *          string      $response['avatar']                 A base64 encoded image to display as the user's avatar
     *          string      $response['avatar_static']
     *          string      $response['header']                 A base64 encoded image to display as the user's header image
     *          string      $response['header_static']
     */
    public function accounts(string $id): array
    {
        return $this->_get('/api/v1/accounts/' . $id);
    }

    /**
     * _get
     *
     * @param string $url
     * @param array $parameters
     *
     * @return  array       $response
     */
    private function _get(string $url, array $parameters = array()): array
    {

        $params["method"] = "GET";
        // set authorization bearer
        if (isset($this->token['access_token'])) {
            $params['headers'] = array(
                'Authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']
            );
        }
        $params['body'] = $parameters;
        $url = $this->mastodon_url . $url;
        return $this->get_content_remote($url, $params);
    }

    /**
     * accounts_verify_credentials
     *
     * Getting the current user
     *
     * @return  array       $response
     */
    public function accounts_verify_credentials(): array
    {
        return $this->_get('/api/v1/accounts/verify_credentials');
    }

    /**
     * accounts_update_credentials
     *
     * Updating the current user
     *
     * @param array $parameters
     *          string      $parameters['display_name']     The name to display in the user's profile
     *          string      $parameters['note']             A new biography for the user
     *          string      $parameters['avatar']           A base64 encoded image to display as the user's avatar
     *          string      $parameters['header']           A base64 encoded image to display as the user's header image
     *
     * @return  array   $response
     */
    public function accounts_update_credentials($parameters): array
    {
        return $this->_patch('/api/v1/accounts/update_credentials', $parameters);
    }

    /**
     * _patch
     *
     * @param string $url
     * @param array $parameters
     *
     * @return  array       $parameters
     */
    private function _patch(string $url, array $parameters = array()): array
    {

        $params["method"] = "PATCH";

        // set authorization bearer
        if (isset($this->token['access_token'])) {
            $params['headers'] = array(
                'Authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']
            );
        }
        $params['body'] = $parameters;

        $url = $this->mastodon_url . $url;
        return $this->get_content_remote($url, $params);
    }

    /**
     * accounts_followers
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_followers(string $id): array
    {
        return $this->_get('/api/v1/accounts/' . $id . '/followers');
    }

    /**
     * accounts_following
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_following(string $id): array
    {
        return $this->_get('/api/v1/accounts/' . $id . '/following');
    }

    /**
     * accounts_statuses
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_statuses(string $id): array
    {
        return $this->_get('/api/v1/accounts/' . $id . '/statuses');
    }

    /**
     * accounts_own_statuses, only own statuses
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_own_statuses(string $id): array
    {
        $response = $this->_get('/api/v1/accounts/' . $id . '/statuses?exclude_replies=1');
        $result = [];
        foreach ($response['html'] as $r) {
            if (is_null($r['reblog'])) {
                $result[] = $r;
            }
        }

        $response['html'] = $result;

        return $response;
    }

    /**
     * accounts_follow
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_follow(string $id): array
    {
        return $this->_post('/api/v1/accounts/' . $id . '/follow');
    }

    /**
     * accounts_unfollow
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_unfollow(string $id): array
    {
        return $this->_post('/api/v1/accounts/' . $id . '/unfollow');
    }

    /**
     * accounts_block
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_block(string $id): array
    {
        return $this->_post('/api/v1/accounts/' . $id . '/block');
    }

    /**
     * accounts_unblock
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_unblock(string $id): array
    {
        return $this->_post('/api/v1/accounts/' . $id . '/unblock');
    }

    /**
     * accounts_mute
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_mute(string $id): array
    {
        return $this->_post('/api/v1/accounts/' . $id . '/mute');
    }

    /**
     * accounts_unmute
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function accounts_unmute(string $id): array
    {
        return $this->_post('/api/v1/accounts/' . $id . '/unmute');
    }

    /**
     * accounts_relationships
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param array $parameters
     *          int         $parameters['id']
     *
     * @return  array       $response
     *          int         $response['id']
     *          bool        $response['following']
     *          bool        $response['followed_by']
     *          bool        $response['blocking']
     *          bool        $response['muting']
     *          bool        $response['requested']
     */
    public function accounts_relationships(array $parameters): array
    {
        return $this->_get('/api/v1/accounts/relationships', $parameters);
    }

    /**
     * accounts_search
     *
     * @param array $parameters
     *          string      $parameters['q']
     *          int         $parameters['limit']        default : 40
     *
     * @return  array       $response
     */
    public function accounts_search(array $parameters): array
    {
        return $this->_get('/api/v1/accounts/search', $parameters);
    }

    /**
     * blocks
     *
     * @return  array       $response
     */
    public function blocks(): array
    {
        return $this->_get('/api/v1/blocks');
    }

    /**
     * favourites
     *
     * @return  array       $response
     */
    public function favourites(): array
    {
        return $this->_get('/api/v1/favourites');
    }

    /**
     * follow_requests
     *
     * @return  array       $response
     */
    public function follow_requests(): array
    {
        return $this->_get('/api/v1/follow_requests');
    }

    /**
     * follow_requests_authorize
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     *
     * @return  array       $response
     */
    public function follow_requests_authorize(string $id): array
    {
        return $this->_post('/api/v1/follow_requests/authorize', array('id' => $id));
    }

    /**
     * follow_requests_reject
     *
     * @see     https://your-domain/web/accounts/:id
     *
     * @param string $id
     * @return  array       $response
     */
    public function follow_requests_reject(string $id): array
    {
        return $this->_post('/api/v1/follow_requests/reject', array('id' => $id));
    }

    /**
     * follows
     *
     * Following a remote user
     *
     * @param string $uri username@domain of the person you want to follow
     * @return  array       $response
     */
    public function follows($uri): array
    {
        return $this->_post('/api/v1/follows', array('uri' => $uri));
    }

    /**
     * mutes
     *
     * Fetching a user's mutes
     *
     * @return  array       $response
     */
    public function mutes(): array
    {
        return $this->_get('/api/v1/mutes');
    }

    /**
     * notifications
     *
     *
     * @param $parameters
     *
     * @return  array   $response
     */
    public function notifications($parameters): array
    {
        $url = '/api/v1/notifications';

        return $this->_get($url, $parameters);
    }

    /**
     * notifications_clear
     *
     * Clearing notifications
     *
     * @return  array   $response
     */
    public function notifications_clear(): array
    {
        return $this->_post('/api/v1/notifications/clear');
    }

    /**
     * get_reports
     *
     * Fetching a user's reports
     *
     * @return  array   $response
     */
    public function get_reports(): array
    {
        return $this->_get('/api/v1/reports');
    }

    /**
     * post_reports
     *
     * Reporting a user
     *
     * @param array $parameters
     *          int     $parameters['account_id']       The ID of the account to report
     *          int     $parameters['status_ids']       The IDs of statuses to report (can be an array)
     *          string  $parameters['comment']          A comment to associate with the report.
     *
     * @return  array   $response
     */
    public function post_reports(array $parameters): array
    {
        return $this->_post('/api/v1/reports', $parameters);
    }

    /**
     * search
     *
     * Searching for content
     *
     * @param array $parameters
     *          string  $parameters['q']            The search query
     *          string  $parameters['resolve']      Whether to resolve non-local accounts
     *
     * @return  array   $response
     */
    public function search(array $parameters): array
    {
        return $this->_get('/api/v1/search', $parameters);
    }

    /**
     * statuses
     *
     * Fetching a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses(string $id): array
    {
        return $this->_get('/api/v1/statuses/' . $id);
    }

    /**
     * statuses_context
     *
     * Getting status context
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_context(string $id): array
    {
        return $this->_get('/api/v1/statuses/' . $id . '/context');
    }

    /**
     * statuses_card
     *
     * Getting a card associated with a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_card(string $id): array
    {
        return $this->_get('/api/v1/statuses/' . $id . '/card');
    }

    /**
     * statuses_reblogged_by
     *
     * Getting who reblogged a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_reblogged_by(string $id): array
    {
        return $this->_get('/api/v1/statuses/' . $id . '/reblogged_by');
    }

    /**
     * statuses_favourited_by
     *
     * Getting who favourited a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_favourited_by(string $id): array
    {
        return $this->_get('/api/v1/statuses/' . $id . '/favourited_by');
    }

    /**
     * post_media
     *
     * @param array $parameters
     *          file        $parameters['file']                 Media file encoded using multipart/form-data
     *          string      $parameters['description']          (optional): A plain-text description of the media for accessibility (max 420 chars)
     *          array       $parameters['focus']                (optional):Two floating points, comma-delimited. See focal points
     *
     * @return  array       $response
     */
    public function post_media(array $parameters): array
    {
        return $this->_post('/api/v1/media', $parameters);
    }

    /**
     * post_media
     *
     * @param string $id
     *          array $parameters
     *          string      $parameters['description']          (optional): A plain-text description of the media for accessibility (max 420 chars)
     *          array       $parameters['focus']                (optional):Two floating points, comma-delimited. See focal points
     *
     * @param $parameters
     * @return  array       $response
     */
    public function update_media(string $id, array $parameters): array
    {
        return $this->_put('/api/v1/media/' . $id, $parameters);
    }

    /**
     * _put
     *
     * HTTP API put
     *
     * @param string $url
     * @param array $parameters
     *
     * @return  array       $response
     */
    private function _put(string $url, array $parameters = array()): array
    {

        $params["method"] = "PUT";
        // set access_token
        if (isset($this->token['access_token'])) {
            $params['headers'] = array(
                'Authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']
            );
        }
        $params['body'] = $parameters;
        $url = $this->mastodon_url . $url;
        return $this->get_content_remote($url, $params);
    }

    /**
     * post_statuses
     *
     * @param array $parameters
     *          string      $parameters['status']               The text of the status
     *          int         $parameters['in_reply_to_id']       (optional): local ID of the status you want to reply to
     *          int         $parameters['media_ids']            (optional): array of media IDs to attach to the status (maximum 4)
     *          string      $parameters['sensitive']            (optional): set this to mark the media of the status as NSFW
     *          string      $parameters['spoiler_text']         (optional): text to be shown as a warning before the actual content
     *          string      $parameters['visibility']           (optional): either "direct", "private", "unlisted" or "public"
     *
     * @return  array       $response
     */
    public function post_statuses(array $parameters): array
    {
        return $this->_post('/api/v1/statuses', $parameters);
    }

    /**
     * delete_statuses
     *
     * Deleting a status
     *
     * @param string $id
     *
     * @return  array   $response       empty
     */
    public function delete_statuses(string $id): array
    {
        return $this->_delete('/api/v1/statuses/' . $id);
    }

    /**
     * _delete
     *
     * @param string $url
     *
     * @return  array       $response
     *

     */
    private function _delete(string $url): array
    {
        $parameters = array();
        $parameters["method"] = "DELETE";

        // set authorization bearer
        if (isset($this->token['access_token'])) {
            $parameters['headers'] = array(
                'Authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']
            );
        }
        $url = $this->mastodon_url . $url;
        return $this->get_content_remote($url, $parameters);
    }

    /**
     * delete_scheduled
     *
     * Deleting a scheduled status
     *
     * @param string $id
     *
     * @return  array   $response       empty
     */
    public function delete_scheduled(string $id): array
    {
        return $this->_delete('/api/v1/scheduled_statuses/' . $id);
    }

    /**
     * statuses_reblog
     *
     * Reblogging a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_reblog(string $id): array
    {
        return $this->_post('/api/v1/statuses/' . $id . '/reblog');
    }

    /**
     * statuses_unreblog
     *
     * Unreblogging a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_unreblog(string $id): array
    {
        return $this->_post('/api/v1/statuses/' . $id . '/unreblog');
    }

    /**
     * statuses_favourite
     *
     * Favouriting a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_favourite(string $id): array
    {
        return $this->_post('/api/v1/statuses/' . $id . '/favourite');
    }

    /**
     * statuses_unfavourite
     *
     * Unfavouriting a status
     *
     * @param string $id
     *
     * @return  array   $response
     */
    public function statuses_unfavourite(string $id): array
    {
        return $this->_post('/api/v1/statuses/' . $id . '/unfavourite');
    }


    /**
     * scheduled_statuses
     *
     *
     * @return  array   $response
     */
    public function get_instance(): array
    {
        return $this->_get('/api/v1/instance');
    }


    /**
     * scheduled_statuses
     *
     * @param array $parameters
     *
     * @return  array   $response
     */
    public function get_scheduled($parameters = array()): array
    {
        return $this->_get('/api/v1/scheduled_statuses/', $parameters);
    }

    /**
     * timelines_home
     *
     * @return  array   $response
     */
    public function timelines_home(): array
    {
        return $this->_get('/api/v1/timelines/home');
    }

    /**
     * timelines_public
     *
     * @param array $parameters
     *          bool    $parameters['local']    Only return statuses originating from this instance
     *
     * @return  array   $response
     */
    public function timelines_public(array $parameters = array()): array
    {
        return $this->_get('/api/v1/timelines/public', $parameters);
    }

    /**
     * timelines_tag
     *
     * @param string $hashtag
     * @param array $parameters
     *          bool        $parameters['local']    Only return statuses originating from this instance
     *
     * @return  array       $response
     */
    public function timelines_tag(string $hashtag, array $parameters = array()): array
    {
        return $this->_get('/api/v1/timelines/tag/' . $hashtag, $parameters);
    }


    /***
     * Custom management added by @tom79 https://github.com/stom79/
     */


    /**
     * getInstanceNodeInfo returns the social network type depending of the hostname
     * @param $host
     * @return string|null
     */
    public function getInstanceNodeInfo(string $host): ?string
    {
        $curl = new Curl();
        $url = "https://" . $host . "/.well-known/nodeinfo";
        $reply = $curl->get($url);

        $responseArray = json_decode($reply->response, true);
        if (empty($responseArray)) {
            $curl = new Curl();
            $url = "https://" . $host . "/api/v1/instance";
            $reply = $curl->get($url);
            $responseArray = json_decode($reply->response, true);
            if (empty($responseArray)) {
                return null;
            } else {
                return "MASTODON";
            }
        } else {
            $url = $responseArray["links"][count($responseArray["links"]) - 1]["href"];
            $curl = new Curl();
            $reply = $curl->get($url);
            $responseArray = json_decode($reply->response, true);
            return strtoupper($responseArray["software"]["name"]);
        }
    }

    /**
     * Update a Mastodon account with new params
     * @param $MastodonAccount MastodonAccount
     * @param $accountParams array
     * @return MastodonAccount
     */
    public function updateAccount(MastodonAccount $MastodonAccount, array $accountParams): MastodonAccount
    {

        $MastodonAccount->setUsername($accountParams['username']);
        $MastodonAccount->setAcct($accountParams['acct']);
        $MastodonAccount->setDisplayName($accountParams['display_name']);
        $MastodonAccount->setLocked($accountParams['locked']);
        $MastodonAccount->setBot($accountParams['bot']);
        $MastodonAccount->setCreatedAt($this->stringToDate($accountParams['created_at']));
        $MastodonAccount->setNote(strip_tags($accountParams['note']));
        $MastodonAccount->setUrl($accountParams['url']);
        $MastodonAccount->setAvatar($accountParams['avatar']);
        $MastodonAccount->setAvatarStatic($accountParams['avatar_static']);
        $MastodonAccount->setHeader($accountParams['header']);
        $MastodonAccount->setHeaderStatic($accountParams['header_static']);
        $MastodonAccount->setFollowersCount($accountParams['followers_count']);
        $MastodonAccount->setFollowingCount($accountParams['following_count']);
        $MastodonAccount->setStatusesCount($accountParams['statuses_count']);

        foreach ($MastodonAccount->getEmojis() as $emoji) {
            $MastodonAccount->removeEmoji($emoji);
        }
        if ($accountParams['emojis'] && count($accountParams['emojis']) > 0) {
            foreach ($accountParams['emojis'] as $_e) {
                $emoji = new Emoji();
                $emoji->setUrl($_e['url']);
                $emoji->setShortcode($_e['shortcode']);
                $emoji->setStaticUrl($_e['static_url']);
                $emoji->setVisibleInPicker($_e['visible_in_picker']);
                $emoji->setMastodonAccount($MastodonAccount);
                $MastodonAccount->addEmoji($emoji);
            }
        }
        foreach ($MastodonAccount->getFields() as $field) {
            $MastodonAccount->removeField($field);
        }
        if ($accountParams['fields'] && count($accountParams['fields']) > 0) {
            foreach ($accountParams['fields'] as $_f) {
                $field = new CustomField();
                $field->setName($_f['name']);
                $field->setValue(strip_tags($_f['value']));
                $field->setVerifiedAt($this->stringToDate($_f['verified_at']));
                $field->setMastodonAccount($MastodonAccount);
                $MastodonAccount->addField($field);
            }
        }
        return $MastodonAccount;
    }

    public function stringToDate(?string $string_date): DateTime
    {
        try {
            return new DateTime($string_date);
        } catch (Exception $e) {
        }
        return new DateTime();
    }

    /**
     * getNotifications Hydrate an array of Notification from API reply
     * @param $notificationParams array
     * @return array
     */
    public function getNotifications(array $notificationParams): array
    {
        $notifications = [];
        foreach ($notificationParams as $notificationParam)
            $notifications[] = $this->getSingleNotification($notificationParam);
        return $notifications;
    }

    /**
     * getSingleNotification Hydrate a Notification from API reply
     * @param $notificationParams
     * @return Notification
     */
    public function getSingleNotification($notificationParams): Notification
    {
        $notification = new Notification();
        $notification->setId($notificationParams['id']);
        $notification->setType($notificationParams['type']);
        $notification->setCreatedAt($this->stringToDate($notificationParams['created_at']));
        $notification->setAccount($this->getSingleAccount($notificationParams['account']));
        if (isset($notificationParams['status']))
            $notification->setStatus($this->getSingleStatus($notificationParams['status']));
        return $notification;
    }

    /**
     * get instance configuration from API reply
     * @param $instantParams array
     * @return Instance
     */
    public function getInstanceConfiguration(array $instantParams): Instance
    {
        $Instance = new Instance();
        $Configuration = new Configuration();
        $Statuses = new Statuses();
        $MediaAttachments = new MediaAttachments();
        $Polls = new Polls();
        if(isset($instantParams['configuration'])) {
            //Dealing with statuses configuration
            if(isset($instantParams['configuration']['statuses'])) {
                $Statuses->setMaxCharacters($instantParams['configuration']['statuses']['max_characters']);
                $Statuses->setMaxMediaAttachments($instantParams['configuration']['statuses']['max_media_attachments']);
                $Statuses->setCharactersReservedPerUrl($instantParams['configuration']['statuses']['characters_reserved_per_url']);
            }
            if(isset($instantParams['configuration']['media_attachments'])) {
                $MediaAttachments->setSupportedMimeTypes($instantParams['configuration']['media_attachments']['supported_mime_types']);
                $MediaAttachments->setImageSizeLimit($instantParams['configuration']['media_attachments']['image_size_limit']);
                $MediaAttachments->setImageMatrixLimit($instantParams['configuration']['media_attachments']['image_matrix_limit']);
                $MediaAttachments->setVideoSizeLimit($instantParams['configuration']['media_attachments']['video_size_limit']);
                $MediaAttachments->setVideoFrameRateLimit($instantParams['configuration']['media_attachments']['video_frame_rate_limit']);
                $MediaAttachments->setVideoMatrixLimit($instantParams['configuration']['media_attachments']['video_matrix_limit']);
            }
            if(isset($instantParams['configuration']['polls'])) {
                $Polls->setMaxOptions($instantParams['configuration']['polls']['max_options']);
                $Polls->setMaxCharactersPerOption($instantParams['configuration']['polls']['max_characters_per_option']);
                $Polls->setMinExpiration($instantParams['configuration']['polls']['min_expiration']);
                $Polls->setMaxExpiration($instantParams['configuration']['polls']['max_expiration']);
            }
        } else if (isset($instantParams['pleroma'])) {
            if (isset($instantParams['poll_limits'])) {
                $Polls->setMaxOptions($instantParams['poll_limits']['max_options']);
                $Polls->setMaxCharactersPerOption($instantParams['poll_limits']['max_option_chars']);
                $Polls->setMinExpiration($instantParams['poll_limits']['min_expiration']);
                $Polls->setMaxExpiration($instantParams['poll_limits']['max_expiration']);
            }
            if(isset($instantParams['max_toot_chars'])) {
                $Statuses->setMaxCharacters($instantParams['max_toot_chars']);
            }
        }
        $Configuration->setStatuses($Statuses);
        $Configuration->setMediaAttachments($MediaAttachments);
        $Configuration->setPolls($Polls);
        $Instance->setConfiguration($Configuration);
        return $Instance;
    }
    /**
     * getSingleAccount Hydrate a MastodonAccount from API reply
     * @param $accountParams array
     * @return MastodonAccount
     */
    public function getSingleAccount(array $accountParams): MastodonAccount
    {

        $MastodonAccount = new MastodonAccount();
        $MastodonAccount->setAccountId($accountParams['id']);
        $MastodonAccount->setUsername($accountParams['username']);
        $MastodonAccount->setAcct($accountParams['acct']);
        $MastodonAccount->setDisplayName($accountParams['display_name']);
        $MastodonAccount->setLocked($accountParams['locked']);
        $MastodonAccount->setBot($accountParams['bot']);
        $MastodonAccount->setCreatedAt($this->stringToDate($accountParams['created_at']));
        $MastodonAccount->setNote(strip_tags($accountParams['note']));
        $MastodonAccount->setUrl($accountParams['url']);
        $MastodonAccount->setAvatar($accountParams['avatar']);
        $MastodonAccount->setAvatarStatic($accountParams['avatar_static']);
        $MastodonAccount->setHeader($accountParams['header']);
        $MastodonAccount->setHeaderStatic($accountParams['header_static']);
        $MastodonAccount->setFollowersCount($accountParams['followers_count']);
        $MastodonAccount->setFollowingCount($accountParams['following_count']);
        $MastodonAccount->setStatusesCount($accountParams['statuses_count']);

        if ($accountParams['emojis'] && count($accountParams['emojis']) > 0) {
            foreach ($accountParams['emojis'] as $_e) {
                $emoji = new Emoji();
                $emoji->setUrl($_e['url']);
                $emoji->setShortcode($_e['shortcode']);
                $emoji->setStaticUrl($_e['static_url']);
                $emoji->setVisibleInPicker($_e['visible_in_picker']);
                $emoji->setMastodonAccount($MastodonAccount);
                $MastodonAccount->addEmoji($emoji);
            }
        }
        if ($accountParams['fields'] && count($accountParams['fields']) > 0) {
            foreach ($accountParams['fields'] as $_f) {
                $field = new CustomField();
                $field->setName($_f['name']);
                $field->setValue(strip_tags($_f['value']));
                $field->setVerifiedAt($this->stringToDate($_f['verified_at']));
                $field->setMastodonAccount($MastodonAccount);
                $MastodonAccount->addField($field);
            }
        }
        if ($accountParams['source'] && $accountParams['source']['privacy']) {
            $MastodonAccount->setDefaultVisibility($accountParams['source']['privacy']);
        } else {
            $MastodonAccount->setDefaultVisibility("public");
        }
        if ($accountParams['source']) {
            $MastodonAccount->setDefaultSensitivity($accountParams['source']['sensitive'] ? 1 : 0);
        } else {
            $MastodonAccount->setDefaultSensitivity(0);
        }
        return $MastodonAccount;
    }

    /**
     * getSingleStatus Hydrate a Status from API reply
     * @param $statusParams array
     * @return Status
     */
    public function getSingleStatus(array $statusParams): Status
    {

        $status = new Status();
        $status->setId($statusParams['id']);
        $status->setUrl($statusParams['url']);
        $status->setUri($statusParams['uri']);
        $status->setAccount($this->getSingleAccount($statusParams['account']));
        $status->setInReplyToId($statusParams['in_reply_to_id']);
        $status->setInReplyToAccountId($statusParams['in_reply_to_account_id']);
        $status->setContent($statusParams['content']);
        $status->setCreatedAt($this->stringToDate($statusParams['created_at']));

        if (isset($statusParams['emojis']) && count($statusParams['emojis']) > 0) {
            $emojis = [];
            foreach ($statusParams['emojis'] as $_e) {
                $emoji = new Emoji();
                $emoji->setUrl($_e['url']);
                $emoji->setShortcode($_e['shortcode']);
                $emoji->setStaticUrl($_e['static_url']);
                $emoji->setVisibleInPicker($_e['visible_in_picker']);
                $emojis[] = $emoji;
            }
            $status->setEmojis($emojis);
        }
        $status->setRepliesCount($statusParams['replies_count']);
        $status->setReblogsCount($statusParams['reblogs_count']);
        $status->setFavouritesCount($statusParams['favourites_count']);
        $status->setReblogged($statusParams['reblogged']);
        $status->setFavourited($statusParams['favourited']);
        $status->setMuted($statusParams['muted']);
        $status->setSensitive($statusParams['sensitive']);
        $status->setSpoilerText($statusParams['spoiler_text']);
        $status->setVisibility($statusParams['visibility']);
        if (isset($statusParams['media_attachments']) && count($statusParams['media_attachments']) > 0) {
            $media_attachments = [];
            foreach ($statusParams['media_attachments'] as $_m) {
                $attachment = new Attachment();
                $attachment->setId($_m['id']);
                $attachment->setUrl($_m['url']);
                $attachment->setType($_m['type']);
                if ($_m['remote_url'])
                    $attachment->setRemoteUrl($_m['remote_url']);
                $attachment->setPreviewUrl($_m['preview_url']);
                if ($_m['text_url'])
                    $attachment->setTextUrl($_m['text_url']);
                $attachment->setMeta(serialize($_m['meta']));
                if ($_m['description'])
                    $attachment->setDescription($_m['description']);
                $media_attachments[] = $attachment;
            }
            $status->setMediaAttachments($media_attachments);
        }
        if (isset($statusParams['mentions']) && count($statusParams['mentions']) > 0) {
            $mentions = [];
            foreach ($statusParams['mentions'] as $_m) {
                $mention = new Mention();
                $mention->setUrl($_m['url']);
                $mention->setAcct($_m['acct']);
                $mention->setUsername($_m['username']);
                $mention->setId($_m['id']);
                $mentions[] = $mention;
            }
            $status->setMentions($mentions);
        }

        if (isset($statusParams['tags']) && count($statusParams['tags']) > 0) {
            $tags = [];
            foreach ($statusParams['tags'] as $_t) {
                $tag = new Tag();
                $tag->setUrl($_t['url']);
                $tag->setName($_t['name']);
                $tag->setHistory(isset($_t['history']) ? $_t['history'] : []);
                $tags[] = $tag;
            }
            $status->setTags($tags);
        }

        if (isset($statusParams['application']) && count($statusParams['application']) == 1) {
            $application = new Application();
            $application->setName($statusParams['application']['name']);
            $application->setWebsite($statusParams['application']['website']);
            $status->setApplication($application);
        }

        if (isset($statusParams['poll'])) {
            $poll = new Poll();
            $poll->setId($statusParams['poll']['id']);
            $options = [];
            if ($poll->getOptions() && count($poll->getOptions()) > 0) {
                foreach ($poll->getOptions() as $_o) {
                    $option = new PollOption();
                    $option->setTitle($_o['title']);
                    $option->setVotesCount($_o['votes_count']);
                }
            }
            $poll->setOptions($options);
            $poll->setExpiresAt($this->stringToDate($statusParams['poll']['expired_at']));
            $poll->setExpired($statusParams['poll']['expired']);
            $poll->setVotesCount($statusParams['poll']['votes_count']);
            if (isset($statusParams['poll']['emojis']) && count($statusParams['poll']['emojis']) > 0) {
                $emojis = [];
                foreach ($statusParams['poll']['emojis'] as $_e) {
                    $emoji = new Emoji();
                    $emoji->setUrl($_e['url']);
                    $emoji->setShortcode($_e['shortcode']);
                    $emoji->setStaticUrl($_e['static_url']);
                    $emoji->setVisibleInPicker($_e['visible_in_picker']);
                    $emojis[] = $emoji;
                }
                $poll->setEmojis($emojis);
            }
            $poll->setVoted($statusParams['poll']['voted']);
            $poll->setMultiple($statusParams['poll']['multiple']);
            $poll->setOwnVotes($statusParams['poll']['own_votes']);
            $status->setPoll($poll);
        }


        $status->setLanguage($statusParams['language']);
        $status->setPinned(isset($statusParams['pinned']));
        if ($statusParams['reblog'])
            $status->setReblog($this->getSingleStatus($statusParams['reblog']));
        return $status;
    }

    /**
     * getStatuses Hydrate an array of Status from API reply
     * @param $statusParams
     * @return array
     */
    public function getStatuses($statusParams): array
    {
        $statuses = [];
        foreach ($statusParams as $statusParam)
            $statuses[] = $this->getSingleStatus($statusParam);
        return $statuses;
    }

    /**
     * getScheduledStatuses Hydrate an array of Scheduled Status from API reply
     * @param $statusParams array
     * @param $account MastodonAccount
     * @return array
     */
    public function getScheduledStatuses(array $statusParams, MastodonAccount $account): array
    {
        $statuses = [];
        foreach ($statusParams as $statusParam)
            $statuses[] = $this->getSingleScheduledStatus($statusParam, $account);
        return $statuses;
    }


    /**
     * getSingleScheduledStatus Hydrate a scheduled Status from API reply
     * @param $statusParams array
     * @param $account MastodonAccount
     * @return Status
     */
    public function getSingleScheduledStatus(array $statusParams, MastodonAccount $account): Status
    {

        $status = new Status();
        $status->setId($statusParams['id']);
        $status->setInReplyToId($statusParams['params']['in_reply_to_id']);
        $status->setContent($statusParams['params']['text']);
        $status->setScheduledAt($this->stringToDate($statusParams['scheduled_at']));
        $status->setAccount($account);
        if (isset($statusParams['emojis']) && count($statusParams['emojis']) > 0) {
            $emojis = [];
            foreach ($statusParams['emojis'] as $_e) {
                $emoji = new Emoji();
                $emoji->setUrl($_e['url']);
                $emoji->setShortcode($_e['shortcode']);
                $emoji->setStaticUrl($_e['static_url']);
                $emoji->setVisibleInPicker($_e['visible_in_picker']);
                $emojis[] = $emoji;
            }
            $status->setEmojis($emojis);
        }
        $status->setSensitive($statusParams['params']['sensitive'] ? 1 : 0);
        $status->setSpoilerText($statusParams['params']['spoiler_text']);
        $status->setVisibility($statusParams['params']['visibility']);
        if (isset($statusParams['media_attachments']) && count($statusParams['media_attachments']) > 0) {
            $media_attachments = [];
            foreach ($statusParams['media_attachments'] as $_m) {
                $attachment = new Attachment();
                $attachment->setId($_m['id']);
                $attachment->setUrl($_m['url']);
                $attachment->setType($_m['type']);
                if ($_m['remote_url'])
                    $attachment->setRemoteUrl($_m['remote_url']);
                $attachment->setPreviewUrl($_m['preview_url']);
                if ($_m['text_url'])
                    $attachment->setTextUrl($_m['text_url']);
                $attachment->setMeta(serialize($_m['meta']));
                if ($_m['description'])
                    $attachment->setDescription($_m['description']);
                else
                    $attachment->setDescription("");
                $media_attachments[] = $attachment;
            }
            $status->setMediaAttachments($media_attachments);
        }
        if (isset($statusParams['mentions']) && count($statusParams['mentions']) > 0) {
            $mentions = [];
            foreach ($statusParams['mentions'] as $_m) {
                $mention = new Mention();
                $mention->setUrl($_m['url']);
                $mention->setAcct($_m['acct']);
                $mention->setUsername($_m['username']);
                $mention->setId($_m['id']);
                $mentions[] = $mention;
            }
            $status->setMentions($mentions);
        }

        if (isset($statusParams['tags']) && count($statusParams['tags']) > 0) {
            $tags = [];
            foreach ($statusParams['tags'] as $_t) {
                $tag = new Tag();
                $tag->setUrl($_t['url']);
                $tag->setName($_t['name']);
                $tag->setHistory(isset($_t['history']) ? $_t['history'] : []);
                $tags[] = $tag;
            }
            $status->setTags($tags);
        }
        return $status;
    }

    /**
     * getSingleAttachment Hydrate an Attachment from API reply
     * @param $mediaParams array
     * @return Attachment
     */
    public function getSingleAttachment(array $mediaParams): Attachment
    {

        $attachment = new Attachment();
        $attachment->setId($mediaParams['id']);
        $attachment->setUrl($mediaParams['url']);
        $attachment->setType($mediaParams['type']);
        if ($mediaParams['remote_url'])
            $attachment->setRemoteUrl($mediaParams['remote_url']);
        $attachment->setPreviewUrl($mediaParams['preview_url']);
        if ($mediaParams['text_url'])
            $attachment->setTextUrl($mediaParams['text_url']);
        $attachment->setMeta(serialize($mediaParams['meta']));
        if ($mediaParams['description'])
            $attachment->setDescription($mediaParams['description']);
        return $attachment;
    }

}
