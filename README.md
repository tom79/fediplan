**If you want to contribute to a _translation_:**[![Crowdin](https://badges.crowdin.net/fediplan/localized.svg)](https://crowdin.com/project/fediplan)

## How to install FediPlan

There are 2 methods to run Fediplan
1. Build and run Docker image
2. Manual install

### 1. Build and run Docker image

#### Required programs
- `git`
- `docker`

#### Steps

1. Install required programs

2. Clone this repo<br>
`git clone https://framagit.org/tom79/fediplan.git fediplan`

3. Build the Docker image<br>
`docker build --tag fediplan fediplan`

4. Run the docker image<br>
`docker run --detach --restart unless-stopped --name fediplan fediplan:latest`

5. Find the IP<br>
`docker inspect fediplan | grep IPAddress`

6. Fediplan should be available at _<ip_address>:8080_

### 2. Manual install

#### Required programs
- `git`
- `php 8.3` with these extensions:

    For Composer
    - `openssl`
    - `phar`
    - `iconv`
    - `xml`
    - `simplexml`
    - `xmlwriter`

    For running
    - `ctype`
    - `curl`
    - `dom`
    - `fpm`
    - `intl`
    - `mbstring`
    - `simplexml`
    - `session`
    - `tokenizer`

- `composer` ([getcomposer.org/download](https://getcomposer.org/download/))

#### Steps

1. Install required programs

2. Clone this repo<br>
`git clone https://framagit.org/tom79/fediplan.git fediplan`

3. Install vendors<br>
`php composer.phar install --optimize-autoloader --working-dir=fediplan`

4. Point your server software to `fediplan/public` folder

### Support My work
[fedilab.app/page/donations](https://fedilab.app/page/donations/)

### Credits
Docker configurations are based on [github.com/TrafeX/docker-php-nginx](https://github.com/TrafeX/docker-php-nginx)

See: [Download Composer](https://getcomposer.org/download/)

**3 - Public directory:**

Your site needs to target /path/to/FediPlan/public

    
#### Support My work at [fedilab.app](https://fedilab.app/page/donations/)