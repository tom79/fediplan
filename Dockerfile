FROM composer as composer

COPY --chown=nobody . /app
RUN composer install --optimize-autoloader --no-interaction --no-progress

FROM alpine:3.19

# Install packages and remove default server definition
RUN apk add --no-cache \
  curl \
  nginx \
  php83 \
  php83-ctype \
  php83-curl \
  php83-dom \
  php83-fpm \
  php83-intl \
  php83-mbstring \
  php83-session \
  php83-tokenizer \
  php83-simplexml \
  supervisor

# Configure nginx - http
COPY docker_config/nginx.conf /etc/nginx/nginx.conf
# Configure nginx - default server
COPY docker_config/conf.d /etc/nginx/conf.d/

# Configure PHP-FPM
ENV PHP_INI_DIR /etc/php83
COPY docker_config/fpm-pool.conf ${PHP_INI_DIR}/php-fpm.d/www.conf
COPY docker_config/php.ini ${PHP_INI_DIR}/conf.d/custom.ini

# Configure supervisord
COPY docker_config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Add application
COPY --chown=nobody --from=composer /app/ /var/www/fediplan/

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody.nobody /var/www/fediplan /run /var/lib/nginx /var/log/nginx

# Create symlink for php
RUN ln -s /usr/bin/php83 /usr/bin/php

# Switch to use a non-root user from here on
USER nobody

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping || exit 1
